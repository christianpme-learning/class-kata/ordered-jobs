package orderedjobs;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class OrderedJobsTest{

    @Test
    public void orderedJobsInitTest(){
        OrderedJobs orderedJobs = new OrderedJobs();
        String actual = orderedJobs.sort();
        assertTrue("".equals(actual));
    }

    @Test
    public void registerJobTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('c');

        Job actual = orderedJobs.jobRegister.get(0);

        assertTrue(actual.getIndependentJob().equals('c'));
        assertNull(actual.getDependentJob());
        assertFalse(actual.hasDependentJob());
    }

    @Test
    public void registerJobsTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('b', 'a');

        Job actual = orderedJobs.jobRegister.get(0);

        assertTrue(actual.getDependentJob().equals('b'));
        assertTrue(actual.getIndependentJob().equals('a'));
        assertTrue(actual.hasDependentJob());
    }

    @Test
    public void sortIndependentJobsTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('a');
        orderedJobs.register('b');
        orderedJobs.register('c');

        String actual = orderedJobs.sort();

        assertTrue("abc".equals(actual));
    }

    @Test
    public void sortDependentJobsTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('b', 'a');
        orderedJobs.register('c', 'b');
        orderedJobs.register('d', 'c');

        String actual = orderedJobs.sort();

        assertTrue("abcd".equals(actual));
    }

    @Test
    public void sortJobsTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('c');
        orderedJobs.register('b', 'a');
        orderedJobs.register('c', 'b');

        String actual = orderedJobs.sort();

        assertTrue("abc".equals(actual));
    }

    @Test
    public void handleDuplicateJobsTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('c');
        orderedJobs.register('b', 'a');
        orderedJobs.register('c'); //duplicate
        orderedJobs.register('c', 'b');
        orderedJobs.register('c'); //duplicate

        String actual = orderedJobs.sort();

        assertTrue("abc".equals(actual));
    }

    @Test
    public void directCircularDependencyTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('b', 'a');
        orderedJobs.register('a', 'b');

        String actual = orderedJobs.sort();

        assertTrue("ab".equals(actual));
        assertTrue(1==orderedJobs.jobRegister.size());
    }

    @Test
    public void indirectCircularDependencyTest(){
        OrderedJobs orderedJobs = new OrderedJobs();

        orderedJobs.register('b', 'a');
        orderedJobs.register('c', 'b');
        orderedJobs.register('a', 'c');

        String actual = orderedJobs.sort();

        assertTrue("abc".equals(actual));
        assertTrue(2==orderedJobs.jobRegister.size());
    }
}