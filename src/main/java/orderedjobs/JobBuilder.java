package orderedjobs;

class JobBuilder{
    private StringBuilder sb = new StringBuilder();

    public JobBuilder append(Job job){
        sb.append(job.toString());
        return this;
    }

    public JobBuilder insert(Job job){
		if (this.contains(job.getIndependentJob()) && !this.contains(job.getDependentJob())) {
			if (lastInsertJob(job.getIndependentJob())) { 
				sb.append(job.getDependentJob());
			} 
			else {
                sb.append(job.getDependentJob().toString(), 
                jobIndex(job.getIndependentJob()) + 1, 
                jobIndex(job.getIndependentJob()) + 2);
			}
		}
		return this;
    }

    private int jobIndex(Character job){
        return sb.indexOf(job.toString());
    }

    private boolean contains(Character job){
        return jobIndex(job) != -1;
    }

    private boolean lastInsertJob(Character job){
        return jobIndex(job) == sb.length() - 1;
    }

    public JobBuilder append(Character c) {
        sb.append(c);
        return this;
    }

    public boolean isEmpty(){
        return sb.toString().isEmpty();
    }

    @Override
    public String toString(){
        return sb.toString();
    }
}