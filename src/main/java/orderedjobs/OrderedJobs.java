package orderedjobs;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

class OrderedJobs {
	private static final Logger log = Logger.getLogger(OrderedJobs.class.getName());

	List<Job> jobRegister = new LinkedList<>();

	public String sort() {
		JobBuilder jb = new JobBuilder();
		for (Job job : jobRegister) {
			if (job.hasDependentJob()) {
				if (jb.isEmpty()) {
					jb.append(job);
				} 
				else{
					jb.insert(job);
				}
			} 
			else {
				if (!dependenceInRegisterFound(job)) {
					jb.append(job.getIndependentJob()); // FIFO
				}
			}
		}
		return jb.toString();
	}

	private boolean dependenceInRegisterFound(Job independentJob) {
		for (Job job : jobRegister) {
			boolean isNotSameJob = !job.equals(independentJob);
			if (isNotSameJob && job.hasDependentJob() && job.getDependentJob().equals(independentJob.getIndependentJob())) {
				return true;
			}
		}
		return false;
	}

	public void register(char independentJob) {
		jobRegister.add(new Job(independentJob));
	}

	public void register(char dependentJob, char independentJob) {
		try {
			circularDependencyDetection(dependentJob, independentJob);
			jobRegister.add(new Job(dependentJob, independentJob));
		} 
		catch (Exception e) {
			log.warning(e.toString());
		}
	}

	private void circularDependencyDetection(Character dependentJob, Character independentJob) throws CircularDependencyFound{
		for(Job job : jobRegister){
			if(job.hasDependentJob() && job.getIndependentJob().equals(dependentJob)){
				if(job.getDependentJob().equals(independentJob)){
					throw new DirectCircularDependencyException();
				}
				else{
					throw new IndirectCircularDependencyException();
				}
			}
		}
	}
}

class CircularDependencyFound extends Exception{
	private static final long serialVersionUID = 1L;
}

class DirectCircularDependencyException extends CircularDependencyFound{
	private static final long serialVersionUID = 1L;
}

class IndirectCircularDependencyException extends CircularDependencyFound{
	private static final long serialVersionUID = 1L;
}