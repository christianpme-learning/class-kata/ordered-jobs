package orderedjobs;

public class Job {
    private Character dependentJob;
    private Character independentJob;

    public Job(char independentJob) {
        this.independentJob = independentJob;
	}

	public Job(char dependentJob, char independentJob) {
        this.dependentJob = dependentJob;
        this.independentJob = independentJob;
	}

	public Character getDependentJob() {
        return dependentJob;
    }

    public void setDependentJob(Character dependentJob) {
        this.dependentJob = dependentJob;
    }

    public Character getIndependentJob() {
        return independentJob;
    }

    public void setIndependentJob(Character independentJob) {
        this.independentJob = independentJob;
    }

	public boolean hasDependentJob() {
		return dependentJob!=null;
    }
    
    @Override
    public String toString(){
        return independentJob.toString()+dependentJob.toString();
    }
}
